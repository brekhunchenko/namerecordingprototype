//
//  MMAudioUtils.h
//  NameRecording
//
//  Created by Yaroslav Brekhunchenko on 9/10/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface MMAudioUtils : NSObject

+ (void)checkStatus:(OSStatus)result
            message:(const char *)message;

+ (SInt16 **)shortBuffersWithNumberOfFrames:(UInt32)frames
                           numberOfChannels:(UInt32)channels;
+ (void)freeShortBuffers:(SInt16 **)buffers
        numberOfChannels:(UInt32)channels;

+ (AudioBufferList *)audioBufferListWithNumberOfFrames:(UInt32)frames
                                      numberOfChannels:(UInt32)channels;
+ (void)freeBufferList:(AudioBufferList *)bufferList;

@end
