//
//  MMNameWaveformView.h
//  NameRecording
//
//  Created by Yaroslav Brekhunchenko on 9/10/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMNameWaveformView : UIView

@property (nonatomic, strong) NSData* waveformData;

@property (nonatomic, assign) Float32 maximumVolume;
@property (nonatomic, assign) Float32 lineWidth;

@end
