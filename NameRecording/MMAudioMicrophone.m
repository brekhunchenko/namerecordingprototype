//
//  MMAudioMicrophone.m
//  NameRecording
//
//  Created by Yaroslav Brekhunchenko on 9/10/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MMAudioMicrophone.h"

#import <AVFoundation/AVFoundation.h>

#import "MMAudioUtils.h"
#import "MMAudioDefines.h"

typedef struct {
  AudioUnit audioUnit;
  AudioBufferList *audioBufferList;
  SInt16 **shortData;
  AudioStreamBasicDescription inputFormat;
  AudioStreamBasicDescription streamFormat;
} MMMicrophoneInfo;

static OSStatus audioMicrophoneCallback(void *inRefCon,
                                        AudioUnitRenderActionFlags *ioActionFlags,
                                        const AudioTimeStamp *inTimeStamp,
                                        UInt32 inBusNumber,
                                        UInt32 inNumberFrames,
                                        AudioBufferList *ioData);


@interface MMAudioMicrophone()

@property (nonatomic, assign) MMMicrophoneInfo *microphoneInfo;

@end

@implementation MMAudioMicrophone

#pragma mark - Initializers

- (instancetype)init {
  self = [super init];
  if (self) {
    self.microphoneInfo = (MMMicrophoneInfo *)malloc(sizeof(MMMicrophoneInfo));
    memset(self.microphoneInfo, 0, sizeof(MMMicrophoneInfo));
    
    [self _setup];
  }
  return self;
}

#pragma mark -

- (void)dealloc {
  [MMAudioUtils checkStatus:AudioUnitUninitialize(self.microphoneInfo->audioUnit)
                    message:"Failed to unintialize audio unit for microphone."];
  [MMAudioUtils freeBufferList:self.microphoneInfo->audioBufferList];
  [MMAudioUtils freeShortBuffers:self.microphoneInfo->shortData
                numberOfChannels:self.microphoneInfo->streamFormat.mChannelsPerFrame];
  free(self.microphoneInfo);
}

#pragma mark - Setup

- (void)_setup {
  AudioComponentDescription inputComponentDescription;
  inputComponentDescription.componentType = kAudioUnitType_Output;
  inputComponentDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
  inputComponentDescription.componentSubType = kAudioUnitSubType_RemoteIO;
  inputComponentDescription.componentFlags = 0;
  inputComponentDescription.componentFlagsMask = 0;
  
  AudioComponent inputComponent = AudioComponentFindNext(NULL , &inputComponentDescription);
  NSAssert(inputComponent, @"Couldn't get input component unit!");
  
  AudioComponentInstanceNew(inputComponent, &self.microphoneInfo->audioUnit);
  
  UInt32 flag = 1;
  [MMAudioUtils checkStatus:AudioUnitSetProperty(self.microphoneInfo->audioUnit,
                                                 kAudioOutputUnitProperty_EnableIO,
                                                 kAudioUnitScope_Input,
                                                 1,
                                                 &flag,
                                                 sizeof(flag))
                    message:"Set kAudioOutputUnitProperty_EnableIO failed."];
  
  UInt32 propSize = sizeof(self.microphoneInfo->inputFormat);
  [MMAudioUtils checkStatus:AudioUnitGetProperty(self.microphoneInfo->audioUnit,
                                                 kAudioUnitProperty_StreamFormat,
                                                 kAudioUnitScope_Input,
                                                 1,
                                                 &self.microphoneInfo->inputFormat,
                                                 &propSize)
                    message:"Set kAudioUnitProperty_StreamFormat failed."];
  
  self.microphoneInfo->inputFormat.mSampleRate = [[AVAudioSession sharedInstance] sampleRate];
  NSAssert(self.microphoneInfo->inputFormat.mSampleRate, @"Expected AVAudioSession sample rate to be greater than 0.0. Did you setup the audio session?");
  
  AudioStreamBasicDescription audioDescription;
  audioDescription.mSampleRate = kSampleRate;
  audioDescription.mFormatID = kAudioFormatLinearPCM;
  audioDescription.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
  audioDescription.mChannelsPerFrame = 1;
  audioDescription.mBitsPerChannel = 16;
  audioDescription.mBytesPerFrame = sizeof(SInt16);
  audioDescription.mBytesPerPacket = sizeof(SInt16);
  audioDescription.mFramesPerPacket = 1;
  
  self.microphoneInfo->streamFormat = audioDescription;
  [MMAudioUtils checkStatus:AudioUnitSetProperty(self.microphoneInfo->audioUnit,
                                                 kAudioUnitProperty_StreamFormat,
                                                 kAudioUnitScope_Input,
                                                 0,
                                                 &audioDescription,
                                                 sizeof(audioDescription))
                    message:"Set kAudioUnitProperty_StreamFormat failed."];
  
  [MMAudioUtils checkStatus:AudioUnitSetProperty(self.microphoneInfo->audioUnit,
                                                 kAudioUnitProperty_StreamFormat,
                                                 kAudioUnitScope_Output,
                                                 1,
                                                 &audioDescription,
                                                 sizeof(audioDescription))
                    message:"Set kAudioUnitProperty_StreamFormat failed."];
  
  AURenderCallbackStruct renderCallbackStruct;
  renderCallbackStruct.inputProc = audioMicrophoneCallback;
  renderCallbackStruct.inputProcRefCon = (__bridge void *)(self);
  [MMAudioUtils checkStatus:AudioUnitSetProperty(self.microphoneInfo->audioUnit,
                                                 kAudioOutputUnitProperty_SetInputCallback,
                                                 kAudioUnitScope_Global,
                                                 1,
                                                 &renderCallbackStruct,
                                                 sizeof(renderCallbackStruct))
                    message:"Set kAudioOutputUnitProperty_SetInputCallback failed."];
  
  [MMAudioUtils checkStatus:AudioUnitInitialize(self.microphoneInfo->audioUnit)
                    message:"Microphone audio unit initilize failed."];
  
  
  UInt32 maximumBufferSize;
  propSize = sizeof(maximumBufferSize);
  [MMAudioUtils checkStatus:AudioUnitGetProperty(self.microphoneInfo->audioUnit,
                                                 kAudioUnitProperty_MaximumFramesPerSlice,
                                                 kAudioUnitScope_Global,
                                                 0,
                                                 &maximumBufferSize,
                                                 &propSize)
                    message:"Failed to get maximum number of frames per slice"];
  
  UInt32 channels = audioDescription.mChannelsPerFrame;
  self.microphoneInfo->shortData = [MMAudioUtils shortBuffersWithNumberOfFrames:maximumBufferSize
                                                               numberOfChannels:channels];
  self.microphoneInfo->audioBufferList = [MMAudioUtils audioBufferListWithNumberOfFrames:maximumBufferSize
                                                                        numberOfChannels:channels];
}

#pragma mark - Custom Setters & Getters

- (void)setEnableMicrophone:(BOOL)enableMicrophone {
  if (_enableMicrophone != enableMicrophone) {
    _enableMicrophone = enableMicrophone;
    if (_enableMicrophone) {
      [MMAudioUtils checkStatus:AudioOutputUnitStart(self.microphoneInfo->audioUnit)
                        message:"Microphone audio unit start failed."];
    } else {
      [MMAudioUtils checkStatus:AudioOutputUnitStop(self.microphoneInfo->audioUnit)
                        message:"Failed to stop microphone audio unit"];
    }
  }
}

#pragma mark - Class Methods

- (AudioStreamBasicDescription)audioStreamBasicDescription {
  return self.microphoneInfo->streamFormat;
}

#pragma mark - Audio Callback

static OSStatus audioMicrophoneCallback(void *inRefCon,
                                        AudioUnitRenderActionFlags *ioActionFlags,
                                        const AudioTimeStamp *inTimeStamp,
                                        UInt32 inBusNumber,
                                        UInt32 inNumberFrames,
                                        AudioBufferList *ioData) {
  MMAudioMicrophone *microphone = (__bridge MMAudioMicrophone *)inRefCon;
  MMMicrophoneInfo *info = (MMMicrophoneInfo *)microphone.microphoneInfo;
  
  for (int i = 0; i < info->audioBufferList->mNumberBuffers; i++) {
    info->audioBufferList->mBuffers[i].mDataByteSize = inNumberFrames*info->streamFormat.mBytesPerFrame;
  }
  
  OSStatus result = AudioUnitRender(info->audioUnit,
                                    ioActionFlags,
                                    inTimeStamp,
                                    inBusNumber,
                                    inNumberFrames,
                                    info->audioBufferList);
  
  if ([microphone.delegate respondsToSelector:@selector(audioMicrophone:recievedBufferList:withBufferSize:)]) {
    [microphone.delegate audioMicrophone:microphone
                      recievedBufferList:info->audioBufferList
                          withBufferSize:inNumberFrames];
  }
  
  return result;
}

@end
