//
//  MMAudioMicrophone.h
//  NameRecording
//
//  Created by Yaroslav Brekhunchenko on 9/10/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@protocol MMAudioMicrophoneDelegate;

@interface MMAudioMicrophone : NSObject

- (instancetype)init;

@property (nonatomic, assign) BOOL enableMicrophone;
@property (nonatomic, weak) id<MMAudioMicrophoneDelegate> delegate;

- (AudioStreamBasicDescription)audioStreamBasicDescription;

@end

@protocol MMAudioMicrophoneDelegate <NSObject>

- (void)audioMicrophone:(MMAudioMicrophone *)microphone
     recievedBufferList:(AudioBufferList *)bufferList
         withBufferSize:(UInt32)bufferSize;

@end
