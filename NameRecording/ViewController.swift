//
//  ViewController.swift
//  NameRecording
//
//  Created by Yaroslav Brekhunchenko on 9/10/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController, MMAudioMicrophoneDelegate {
  var microphone: MMAudioMicrophone!;
  var nameWaveformData: NSMutableData!;
  
  @IBOutlet weak var nameWaveformView: MMNameWaveformView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    microphone = MMAudioMicrophone();
    microphone.delegate = self;
  }


  @IBAction func pressToRecordButtonTouchDown(_ sender: Any) {
    nameWaveformData = NSMutableData();
    microphone?.enableMicrophone = true;
  }
  
  @IBAction func pressToRecordButtonTouchUp(_ sender: Any) {
    microphone?.enableMicrophone = false;
  }
    
  @IBAction func volumeSliderValueChanged(_ sender: UISlider) {
    nameWaveformView?.maximumVolume = sender.value;
  }
  
  func audioMicrophone(_ microphone: MMAudioMicrophone!, recievedBufferList bufferList: UnsafeMutablePointer<AudioBufferList>!, withBufferSize bufferSize: UInt32) {
    DispatchQueue.main.async {
      let length = bufferList[0].mBuffers.mDataByteSize
      let data = NSData(bytes: bufferList[0].mBuffers.mData, length: Int(length))
      self.nameWaveformData.append(data as Data)
      self.nameWaveformView?.waveformData = self.nameWaveformData as Data!;
    }
  }
}
