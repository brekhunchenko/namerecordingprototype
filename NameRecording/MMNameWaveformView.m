//
//  MMNameWaveformView.m
//  NameRecording
//
//  Created by Yaroslav Brekhunchenko on 9/10/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MMNameWaveformView.h"
#import "MMAudioDefines.h"

@implementation MMNameWaveformView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _maximumVolume = 160;
        _lineWidth = 2.5;
    }
    return self;
}

- (void)setLineWidth:(Float32)lineWidth {
    _lineWidth = lineWidth;
    
    [self setNeedsDisplay];
}

- (void)setMaximumVolume:(Float32)maximumVolume {
    _maximumVolume = maximumVolume;
    
    [self setNeedsDisplay];
}

- (void)setWaveformData:(NSData *)waveformData {
  _waveformData = waveformData;
  
  [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
  if (_waveformData) {
    SInt16 *waveform = (SInt16 *)[_waveformData bytes];
    NSUInteger length = [_waveformData length]/sizeof(SInt16);
    
    Float32 maxValue = _maximumVolume;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [[UIColor blackColor] CGColor]);
    CGContextSetLineWidth(context, _lineWidth);
    
    int maxLength = kSampleRate*2; //two seconds of audio is max
    for (int i = 0; i < maxLength; i++) {
      if (i < length) {
        CGFloat x = rect.size.width*((float)i/maxLength);
        CGFloat y = MIN(rect.size.height, MAX(0.0, rect.size.height/2.0f - rect.size.height/2.0f*((Float32)waveform[i]/maxValue)));
        if (i == 0) {
          CGContextMoveToPoint(context, x, y);
        } else {
          CGContextAddLineToPoint(context, x, y);
        }
      }
    }
    
    CGContextDrawPath(context, kCGPathStroke);
    
  }
}

@end
